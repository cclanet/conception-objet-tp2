from tp2 import *
#creer des boites
def test_creer_des_boites():
	b=Box()
#mettre des trucs dans les boites

def test_mettre_des_truc_dans_des_boites():
	b=Box()
	b.open()

	b.ajout("truc1")
	b.ajout("truc2")
	
#Un truc ajoute a une boite est dans la boite

def test_truc_bien_dans_la_boite():
	b=Box()
	b.open()
	b.ajout("truc1")
	b.ajout("truc2")
	assert b.is_in("truc1")
	assert b.is_in("truc2")

#Un truc pas ajoute a une boite est pas dans la boite

def test_truc_pas_dans_la_boite():
	b=Box()
	b.open()
	assert not b.is_in("truc1")
	b.ajout("truc1")
	b.ajout("truc2")
	assert b.is_in("truc1")
	assert b.is_in("truc2")

#rendre contents prive

#verifier si truc dans boite

def test_verifier_si_truc_dans_boite():
	b=Box()
	b.open()
	assert not b.is_in("truc1")
	b.ajout("truc1")
	assert not b.is_in("truc2")
	b.ajout("truc2")
	b.action_look()
	assert b.is_in("truc1")
	assert b.is_in("truc2")
	b.delete("truc1")
	assert not b.is_in("truc1")
	
#boite ouverte ou fermee
def test_ouverture():
	b=Box()
	assert not b.isOpen()
	b.open()
	assert  b.isOpen()
	b.close()
	assert not b.isOpen()

#voircequilyadedans

def test_vue():
	b=Box()
	b.open()
	assert not b.is_in("truc1")
	b.ajout("truc1")
	assert not b.is_in("truc2")
	b.ajout("truc2")
	assert b.is_in("truc1")
	assert b.is_in("truc2")
	b.action_look()

def test_ajoutpoids():
	t = Thing(3)
	assert t.volume() == 3

def test_setcle():
	b = Box()
	t = Thing()
	t.set_name("carte")
	b.set_key(t)
	b.open()
	assert not b.isOpen()
	b.open_with(t)
	assert b.isOpen()

def test_player():

	b = Box()

	p = Player("joueur", 50)

	assert p.name == "joueur"
	assert p.get_capacity() == 50

	p.set_name("Mon joueur")
	p.set_capacity(150)

	assert p.name == "Mon joueur"
	assert p.get_capacity() == 150

	# Ajout joueur boite
	b.ajout(p)
	assert b.is_in(p)

def test_ajout_chose_container():

	b = Box()
	t = Thing()
	assert t.container == None
	b.ajout(t)
	assert t.container == b




