class Box():

	def __init__(self):
		self.contents = []
		self.ouverture = False
		self.capacity = None
		self.key = None

	def __contains__(self, truc):
		return self.is_in(truc)

	def ajout(self, truc):
		self.contents.append(truc)
		if type(truc) is Thing:
			truc.updateBox(self)

	def is_in(self, truc):
		return truc in self.contents

	def delete(self, truc):
		self.contents.remove(truc)

	def isOpen(self):
		return self.ouverture

	def close(self):
		self.ouverture = False

	def open(self):
		if self.key == None:
			self.ouverture = True

	def action_look(self):
		res = "la boite est fermee"
		if self.isOpen():
			res = "la boite contient: " + ", ".join(self.contents)
		return res

	def set_capacity(self, num):
		self.capacity = num

	def capacity(self):
		return self.capacity

	def has_room_for(self, thing):
		return self.capacity() == None or thing.volume() <= self.capacity()
		
	def action_add(self, thing):
		res = False
		if self.has_room_for(thing) and self.isOpen():
			self.ajout(thing)
			if self.capacity() != None:
				self.set_capacity(self.capacity() - thing.volume())
			res = True
		return res

	def find(self, thing):
		res = None
		for t in self.contents:
			if t.name == thing: res = t
		return res

	def set_key(self, t):
		self.key = t

	def open_with(self, t):
		if t == self.key:
			self.ouverture = True


class Thing():

	def __init__(self, place = None, name = None):
		self.place = place
		self.name = name
		self.container = None

	def __repr__(self):
		return self.name

	def volume(self):
		return self.place

	def set_name(self, name):
		self.name = name 

	def has_name(self, name):
		return self.name == name

	def updateBox(self, box):
		self.container = box

class Player:

	name = None
	inventaire = Box()

	def __init__(self, nom, capa):
		self.set_name(nom)
		self.set_capacity(capa)

	def set_name(self, newName):
		self.name = newName

	def set_capacity(self, newCapa):
		self.capacity = newCapa
		self.inventaire.set_capacity(newCapa)

	def get_capacity(self):
		return self.inventaire.capacity


